use std::fmt;

type GenError = Box<dyn std::error::Error>;

#[derive(Debug)]
enum BrainfuckInstrRaw {
    Plus,
    Minus,
    LessThan,
    GreaterThan,
    LeftBracket,
    RightBracket,
    Comma,
    Fullstop,
}

impl BrainfuckInstrRaw {
    fn from_byte(c: u8) -> Option<BrainfuckInstrRaw> {
        match c {
            b'+' => Some(BrainfuckInstrRaw::Plus),
            b'-' => Some(BrainfuckInstrRaw::Minus),
            b'<' => Some(BrainfuckInstrRaw::LessThan),
            b'>' => Some(BrainfuckInstrRaw::GreaterThan),
            b'[' => Some(BrainfuckInstrRaw::LeftBracket),
            b']' => Some(BrainfuckInstrRaw::RightBracket),
            b',' => Some(BrainfuckInstrRaw::Comma),
            b'.' => Some(BrainfuckInstrRaw::Fullstop),
            _ => None,
        }
    }
}

#[derive(Debug)]
struct BrainfuckInstr {
    instr: BrainfuckInstrRaw,
    line: usize,
    column: usize,
}

impl BrainfuckInstr {
    fn from_file(fname: &str) -> Result<Vec<BrainfuckInstr>, GenError> {
        let content = std::fs::read_to_string(fname).expect("Failed to read_to_string");

        let mut program: Vec<BrainfuckInstr> = Vec::new();

        for (l, pline) in content.lines().enumerate() {
            for (c, pbyte) in pline.bytes().enumerate() {
                if let Some(iraw) = BrainfuckInstrRaw::from_byte(pbyte) {
                    program.push(BrainfuckInstr {
                        instr: iraw,
                        line: l,
                        column: c,
                    });
                }
            }
        }

        Ok(program)
    }

    /// Returns the line number, starting at 1
    fn line1(&self) -> usize {
        self.line + 1
    }
}

impl fmt::Display for BrainfuckInstr {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let text = match self.instr {
            BrainfuckInstrRaw::Plus => "Increment byte at data pointer",
            BrainfuckInstrRaw::Minus => "Decrement byte at data pointer",
            BrainfuckInstrRaw::LessThan => "Decrement data pointer",
            BrainfuckInstrRaw::GreaterThan => "Increment data pointer",
            BrainfuckInstrRaw::LeftBracket => "Start looping",
            BrainfuckInstrRaw::RightBracket => "End looping",
            BrainfuckInstrRaw::Comma => "Input byte at the data pointer",
            BrainfuckInstrRaw::Fullstop => "Output byte at data pointer",
        };
        write!(f, "{}", text)
    }
}

fn main() -> Result<(), GenError> {
    let fname = std::env::args().nth(1).ok_or("No arg")?;
    let program = BrainfuckInstr::from_file(&fname)?;
    for instr in program {
        // Do I need to implement a getter for instr.column?
        println!("[{}:{}:{:}] {}", fname, instr.line1(), instr.column, instr);
    }
    Ok(())
}
